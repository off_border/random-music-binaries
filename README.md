# README #

This is a simple application that you can use to randomly choose music files from a folders in your computer and create a playlist without effort

**Latest version: 2.9**  (*[Download](https://sourceforge.net/projects/random-the-music/files/latest/download)*)

*Readme written by Mihaela Citea. Edited by Tehnofob.*

Random The Music! is a simple, yet useful application that is capable of randomly selecting music, video or other files from a specific location and create a playlist without requiring user intervention.

### Randomly selects songs to include in a playlist ###

The application, for example, proves useful in situations when you simply can't decide what kind of music you want to listen to. It can pick songs from one or more user-defined folders and copy them to a target directory, so that you save the time needed to manually sort and select music files.

### Simplistic approach ###

Working with this program is a breeze, thanks to its minimalistic interface. The GUI consists of a single window where you can easily choose the folders to browse and view all the audio files they contain. The application can process any type of file you want, provided you enter the desired formats in the designated area of the main window, separated by space.

### Limits the size of the copied files ###

One useful feature is the possibility to limit the total size of the copied files. Thereby, you don't have to worry that Random The Music! will occupy a lot of storage space through its actions. 

Your only task is to press the 'Scan folders' button and select the output destination. Depending on the size limitation and the content of the input folders, the application might take more or less time to complete its task.

### A handy tool to randomly generate a playlist ###

Random The Music! provides you with a way to select the songs to include in your playlist, which comes in handy especially if you have a large, disorganized audio collection. It can randomly select songs to include in a playlist, so you don't have to do this operation manually.

In addition, the application can remember already copied files, and do not use them in the next time. So you can generate randomly, but unique playlists as many times, as you library allow.

### Downloads ###
   You can download the latest version of the application [there](https://sourceforge.net/projects/random-the-music/files/latest/download)

### Issues ###
   Please post about program issues [there](https://sourceforge.net/projects/random-the-music/support)


### Changes ###

**Since 2.8**

+ now picks up .cue sheet files.

- fixed bug with copying to an unexisting destination directory.

**Since 2.7**

+ settings: now you can choose directory to store the list of copied files. Maybe useful after the system reinstalling.


**Since 2.6**

+ remembering copied files by hash (last modification time, size, name). Filepath-independent.

- some grammar fixes


**Since 2.5**

- left click to open file/folder, right to mark file as used/unused ( or delete from search paths list )

+ more text hints during loading search results

- FIXED: drag'n'drop greeting visible now

- FIXED: program crashed when trying to delete folder from search paths list 


**Since 2.3**

+ upgraded user interface

+ restoring previous window state

+ right click to open file or folder with default application

+ left click on track to mark it as "listented"/"not-listened"

- FIXED: window maximizing button

- forced proccess stopping on window close.


**Since 1.4.b**

+ new user interface. Just drag'n'drop your music in window.


**Since 1.0b**

+ program startup speed increased

+ the program now can do not forget used files on rescan